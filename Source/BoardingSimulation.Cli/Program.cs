﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace BoardingSimulation.Cli
{
    class Program
    {
        static void Main(string[] args)
        {
            var simulationCount = 1000;
            _expiriments = Enumerable.Range(1, 9)
                .Select(i => new Expiriment(i, simulationCount))
                .Concat(Enumerable.Range(1, 9)
                .Select(i => new Expiriment(i * 10, simulationCount)))
                .Concat(Enumerable.Range(1, 10)
                .Select(i => new Expiriment(i * 100, simulationCount))).ToList();

            _expiriments.ForEach(e =>
            {
                e.Execute();
                PrintExpiriments();
            });
        }

        private static List<Expiriment> _expiriments = new List<Expiriment>();

        private static void PrintExpiriments()
        {
            Console.SetCursorPosition(0, 0);
            var sb = new StringBuilder();
            sb.AppendLine("#P\tA\tNA\tTotal");
            _expiriments.ForEach(e =>
                sb.AppendFormat("{0}\t{1}\t{2}\t{3}\n", e.PassengerCount, e.AvailableCount, e.UnavailableCount, e.TotalCount));
            Console.Write(sb);
        }


    }
}
