﻿using System.Linq;

namespace BoardingSimulation
{
    public class BoardingSimulation
    {
        private readonly int _passengerCount;

        public BoardingSimulation(int passengerCount)
        {
            _passengerCount = passengerCount;
        }

        public SimulationResult Execute()
        {
            var airplane = Airplane.Create(_passengerCount);
            var passengers = Enumerable.Range(0, _passengerCount)
                .Select(i => new Passenger(i)).ToList();
            var lastPassenger = passengers.Last();
            var firstPassengers = passengers.Take(_passengerCount - 1).ToList();
            firstPassengers.RandomAction(p => p.IsCrazy = true);
            firstPassengers.ForEach(p => p.Board(airplane));
            var properSeat = airplane.HasAssignedSeatAvailable(lastPassenger.SeatAssignment);
            return new SimulationResult
            {
                ProperSeatAvailable = properSeat
            };
        }
    }
}