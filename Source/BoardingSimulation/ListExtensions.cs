using System;
using System.Collections.Generic;
using System.Linq;

namespace BoardingSimulation
{
    public static class ListExtensions
    {
        private static readonly Random Random = new Random();
        private static readonly object SyncLock = new object();
        private static int RandomNumber(int min, int max)
        {
            lock (SyncLock)
            { // synchronize
                return Random.Next(min, max);
            }
        }

        public static void RandomAction<T>(this IList<T> list, Action<T> modifyAction)
        {
            if (list != null && list.Any())
            {
                var randomIndex = RandomNumber(0, list.Count);
                modifyAction(list[randomIndex]);
            }
        }
    }
}