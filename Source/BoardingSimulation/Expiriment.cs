namespace BoardingSimulation.Cli
{
    public class Expiriment
    {
        public Expiriment(int passengerCount, int simulationCount)
        {
            PassengerCount = passengerCount;
            SimulationCount = simulationCount;
            AvailableCount = 0;
            UnavailableCount = 0;
        }

        public int SimulationCount { get; set; }
        public int PassengerCount { get; set; }
        public int AvailableCount { get; set; }
        public int UnavailableCount { get; set; }
        public int TotalCount { get { return AvailableCount + UnavailableCount; } }

        public void Execute()
        {
            while (TotalCount < SimulationCount)
            {
                var simulation = new BoardingSimulation(PassengerCount);
                var result = simulation.Execute();
                if (result.ProperSeatAvailable)
                    AvailableCount++;
                else
                    UnavailableCount++;
            }
        }

    }
}