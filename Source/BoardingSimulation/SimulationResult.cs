﻿namespace BoardingSimulation
{
    public class SimulationResult
    {
        public bool ProperSeatAvailable { get; set; }
    }
}