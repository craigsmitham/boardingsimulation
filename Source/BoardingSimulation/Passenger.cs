﻿using System;
using System.Dynamic;
using System.Text;
using System.Threading.Tasks;

namespace BoardingSimulation
{
    public class Passenger
    {
        public Passenger(int seatAssignment)
        {
            PassengerId = Guid.NewGuid();
            SeatAssignment = seatAssignment;
        }

        public bool IsCrazy { get; set; }
        public Guid PassengerId { get; private set; }
        public int SeatAssignment { get; private set; }

        public void Board(Airplane airplane)
        {
            if (IsCrazy || !airplane.HasAssignedSeatAvailable(SeatAssignment))
                airplane.SeatInRandomAvailableSeat(PassengerId);
            else
                airplane.SeatInAssignedSeat(this);
        }
    }
}
