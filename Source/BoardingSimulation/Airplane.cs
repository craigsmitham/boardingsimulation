﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BoardingSimulation
{
    public class Airplane
    {
        private List<Seat> _seats;
        private Airplane()
        {
            _seats = new List<Seat>();
        }

        private class Seat
        {
            public Seat(int seatId)
            {
                SeatId = seatId;
            }
            public int SeatId { get; set; }
            public Guid? PassengerId { get; set; }
        }

        public static Airplane Create(int passengerCount)
        {
            var airplane = new Airplane
            {
                _seats = Enumerable.Range(0, passengerCount).Select(i => new Seat(i)).ToList()
            };
            return airplane;
        }

        public bool HasAssignedSeatAvailable(int seatId)
        {
            return _seats.Any(s => s.SeatId == seatId && !s.PassengerId.HasValue);
        }

        public void SeatInAssignedSeat(Passenger passenger)
        {
            SeatInSeat(passenger.SeatAssignment, passenger);
        }

        private void SeatInSeat(int seatId, Passenger passenger)
        {
            _seats.First(s => s.SeatId == seatId).PassengerId = passenger.PassengerId;
        }

        public void SeatInRandomAvailableSeat(Guid passengerId)
        {
            var availableSeats = _seats.Where(s => !s.PassengerId.HasValue).ToList();
            availableSeats.RandomAction(s => s.PassengerId = passengerId);
        }
    }
}